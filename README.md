# website

Here is where the code is kept for my personal website.

There are a couple other branches that are used for different subdomains of my website. The main ones are [urlshortener](https://gitlab.com/Nathaniel.Belles/website/-/tree/urlshortener), which handles all of my url redirects and is used for keeping qr codes small, and [qr](https://gitlab.com/Nathaniel.Belles/website/-/tree/qr) which computes a qr code upon post request and responds with the image file. I hope to add more to these projects in the future. 

For hosting these websites, I use a product called [Vercel](https://vercel.com) which allows you to host static websites for free and links them directly to git repositories. This makes it super easy to publish your code and update your websites, allowing as many dev branches and deployments as you want. 