export default (req, res) => {
    res.statusCode = 200
    res.json({
        ip: req.headers["x-forwarded-for"],
        realip: req.headers["x-real-ip"],
        vercelip: req.headers["x-vercel-forwarded-for"],
        vercelcountry: req.headers["x-vercel-ip-country"],
        vercelstate: req.headers["x-vercel-ip-country-region"],
        vercelcity: req.headers["x-vercel-ip-city"]
    })
}